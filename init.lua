
local uses = 45000

local max_speed = tonumber(minetest.settings:get("toolranks_speed_multiplier")) or 2.0
local max_use = tonumber(minetest.settings:get("toolranks_use_multiplier")) or 2.0
local max_level = tonumber(minetest.settings:get("toolranks_levels")) or 10
local level_digs = tonumber(minetest.settings:get("toolranks_level_digs")) or 500
local level_multiplier = 1 / max_level

local toolrank_toolcap = function(stack)
    if toolranks == nil then
        return stack
    end
    local m = stack:get_meta()
    local idef = stack:get_definition()
    local dugnodes = tonumber(m:get_string("dug")) or 0
    local lastlevel = tonumber(m:get_string("lastlevel")) or 0
    local level = toolranks.get_level(dugnodes)
    if idef.tool_capabilities then
        local speed_multiplier = 1 + (level * level_multiplier * (max_speed - 1))
        local use_multiplier = 1 + (level * level_multiplier * (max_use - 1))
        local caps = table.copy(idef.tool_capabilities)

        caps.full_punch_interval = caps.full_punch_interval and (caps.full_punch_interval / speed_multiplier)
        caps.punch_attack_uses = caps.punch_attack_uses and (caps.punch_attack_uses * use_multiplier)

        for _,c in pairs(caps.groupcaps) do
          c.uses = c.uses * use_multiplier
          for i,t in ipairs(c.times) do
            c.times[i] = t / speed_multiplier
          end
        end
        m:set_tool_capabilities(caps)
    end
    return stack
end

local switch = function (stack, player, target, pointed)
    -- Gather as much info as we can
    local node = minetest.get_node_or_nil(target)
    if node == nil then
        return
    end
    local nodedef = minetest.registered_nodes[node.name] or nil
    if nodedef == nil then
        return
    end
    local nodemeta = minetest.get_meta(target)
    if nodemeta == nil then
        return
    end
    -- Try to not switch on doors and chests and such
    if nodedef.on_rightclick ~= nil then
        return nodedef.on_rightclick(target, node, player, stack, pointed)
    end
    -- Switch only when needed
    local oldmeta = stack:get_meta()
    local change = nil
    local change_into = nil
    local me = stack:get_name()
    local wear = stack:get_wear()
    if (nodedef.groups.cracky ~= nil or nodedef.groups.pickaxey ~= nil or nodedef.groups.invisible_wall == 1) and me ~= "omni_tool:pickaxe" then
        change = ItemStack("omni_tool:pickaxe")
        change_into = "pickaxe"
    elseif (nodedef.groups.choppy ~= nil or nodedef.groups.axey ~= nil) and me ~= "omni_tool:axe" then
        change = ItemStack("omni_tool:axe")
        change_into = "axe"
    elseif (nodedef.groups.crumbly ~= nil or nodedef.groups.shovely ~= nil) and me ~= "omni_tool:shovel" then
        change = ItemStack("omni_tool:shovel")
        change_into = "shovel"
    end
    if change ~= nil then
        if minetest.get_modpath("toolranks") then
            local newmeta = change:get_meta()
            newmeta:set_string("dug", oldmeta:get_string("dug"))
            newmeta:set_string("lastlevel", oldmeta:get_string("lastlevel"))
            if change_into == "pickaxe" then
                newmeta:set_string("description", toolranks.create_description("Omni Tool\nPickaxe", tonumber(newmeta:get_string("dug")) or 0))
            elseif change_into == "axe" then
                newmeta:set_string("description", toolranks.create_description("Omni Tool\nAxe", tonumber(newmeta:get_string("dug")) or 0))
            elseif change_into == "shovel" then
                newmeta:set_string("description", toolranks.create_description("Omni Tool\nShovel", tonumber(newmeta:get_string("dug")) or 0))
            end
        end
        change:set_wear(wear)
        change = toolrank_toolcap(change)
        stack:replace(change)
    end
    return stack
end

local on_sec_place = function (stack, placer, pointed)
    -- Cycle thru
    local change = nil
    local oldmeta = stack:get_meta()
    local me = stack:get_name()
    local wear = stack:get_wear()
    if me == "omni_tool:axe" then
        change = ItemStack("omni_tool:pickaxe 1 " .. tostring(stack:get_wear()))
        local newmeta = change:get_meta()
        if toolranks ~= nil then
            newmeta:set_string("dug", oldmeta:get_string("dug"))
            newmeta:set_string("lastlevel", oldmeta:get_string("lastlevel"))
            newmeta:set_string("description", toolranks.create_description("Omni Tool\nPickaxe", tonumber(newmeta:get_string("dug")) or 0))
        end
        change = toolrank_toolcap(change)
        stack:replace(change)
        return stack
    elseif me == "omni_tool:pickaxe" then
        change = ItemStack("omni_tool:shovel 1 " .. tostring(stack:get_wear()))
        local newmeta = change:get_meta()
        if toolranks ~= nil then
            newmeta:set_string("dug", oldmeta:get_string("dug"))
            newmeta:set_string("lastlevel", oldmeta:get_string("lastlevel"))
            newmeta:set_string("description", toolranks.create_description("Omni Tool\nShovel", tonumber(newmeta:get_string("dug")) or 0))
        end
        change = toolrank_toolcap(change)
        stack:replace(change)
        return stack
    elseif me == "omni_tool:shovel" then
        change = ItemStack("omni_tool:axe 1 " .. tostring(stack:get_wear()))
        local newmeta = change:get_meta()
        if toolranks ~= nil then
            newmeta:set_string("dug", oldmeta:get_string("dug"))
            newmeta:set_string("lastlevel", oldmeta:get_string("lastlevel"))
            newmeta:set_string("description", toolranks.create_description("Omni Tool\nAxe", tonumber(newmeta:get_string("dug")) or 0))
        end
        change = toolrank_toolcap(change)
        stack:replace(change)
        return stack
    end
end

local on_place = function (stack, placer, pointed)
    local change = nil
    local oldmeta = stack:get_meta()
    -- Switch to our axe on combat
    if pointed.type ~= "node" and stack:get_name() ~= "omni_tool:axe" then
        change = ItemStack("omni_tool:axe 1 " .. tostring(stack:get_wear()))
        local newmeta = change:get_meta()
        if toolranks ~= nil then
            newmeta:set_string("dug", oldmeta:get_string("dug"))
            newmeta:set_string("lastlevel", oldmeta:get_string("lastlevel"))
            newmeta:set_string("description", toolranks.create_description("Omni Tool\nAxe", tonumber(newmeta:get_string("dug")) or 0))
        end
        change = toolrank_toolcap(change)
        stack:replace(change)
        return stack
    elseif pointed.type ~= "node" and stack:get_name() == "omni_tool:axe" then
        return
    end
    -- Cycle thru when sneaking
    local ctrl = placer:get_player_control()
    if ctrl.sneak then
        return on_sec_place(stack, placer, pointed)
    end
    -- Detect what node it might be and select the right tool
    return switch(stack, placer, pointed.under, pointed)
end

minetest.register_tool("omni_tool:axe", {
    short_description = "Omni Tool",
    description = "Omni Tool\nAxe",
    inventory_image = "omni_tool_axe.png",
    groups = { tool=1, axe=1, dig_speed_class=5, enchantability=10 },
    tool_capabilities = {
        full_punch_interval = 1.0,
        max_drop_level = 5,
        punch_attack_uses = uses,
        groupcaps = {
            choppy = {
                maxlevel = 3,
                uses = uses,
                times = { [1]=2.10, [2]=0.90, [3]=0.50 }
            },
        },
        damage_groups = {fleshy=9},
    },
    sound = { breaks = "default_tool_breaks" },
    _mcl_toollike_wield = true,
    _mcl_diggroups = {
        axey = { speed = 8, level = 5, uses = uses }
    },
    on_place = on_place,
    on_secondary_use = on_sec_place,
})

minetest.register_tool("omni_tool:pickaxe", {
    short_description = "Omni Tool",
    description = "Omni Tool\nPickaxe",
    inventory_image = "omni_tool_pickaxe.png",
    groups = { tool=1, pickaxe=1, dig_speed_class=5, enchantability=10 },
    tool_capabilities = {
        full_punch_interval = 0.83333333,
        max_drop_level = 5,
        punch_attack_uses = uses,
        groupcaps = {
            cracky = {
                maxlevel = 3,
                uses = uses,
                times = { [1]=2.00, [2]=1.0, [3]=0.50 }
            },
            invisible_wall = {
                maxlevel = 1,
                uses = uses,
                times = { [1]=0.50 }
            },
        },
        damage_groups = {fleshy=5},
    },
    sound = { breaks = "default_tool_breaks" },
    _mcl_toollike_wield = true,
    _mcl_diggroups = {
        pickaxey = { speed = 8, level = 5, uses = uses }
    },
    on_place = on_place,
    on_secondary_use = on_sec_place,
})

minetest.register_tool("omni_tool:shovel", {
    short_description = "Omni Tool",
    description = "Omni Tool\nShovel",
    inventory_image = "omni_tool_shovel.png",
    groups = { tool=1, shovel=1, dig_speed_class=5, enchantability=10 },
    tool_capabilities = {
        full_punch_interval = 0.83333333,
        max_drop_level = 5,
        punch_attack_uses = uses,
        groupcaps = {
            crumbly = {
                maxlevel = 3,
                uses = uses,
                times = { [1]=1.10, [2]=0.50, [3]=0.30 }
            },
        },
        damage_groups = {fleshy=5},
    },
    sound = { breaks = "default_tool_breaks" },
    _mcl_toollike_wield = true,
    _mcl_diggroups = {
        shovely = { speed = 8, level = 5, uses = uses }
    },
    on_place = on_place,
    on_secondary_use = on_sec_place,
})

if minetest.get_modpath("toolranks") then
    minetest.override_item("omni_tool:axe", {
        original_description = "Omni Tool\nAxe",
        description = toolranks.create_description("Omni Tool\nAxe"),
        after_use = toolranks.new_afteruse
    })
    minetest.override_item("omni_tool:pickaxe", {
        original_description = "Omni Tool\nPickaxe",
        description = toolranks.create_description("Omni Tool\nPickaxe"),
        after_use = toolranks.new_afteruse
    })
    minetest.override_item("omni_tool:shovel", {
        original_description = "Omni Tool\nShovel",
        description = toolranks.create_description("Omni Tool\nShovel"),
        after_use = toolranks.new_afteruse
    })
end

local pick = "default:pick_diamond"
local axe = "default:axe_diamond"
local shovel = "default:shovel_diamond"
local fuse = "default:mese_crystal"
local air = ""
if minetest.registered_nodes["mcl_core:stone"] ~= nil then
    pick = "mcl_core:pick_diamond"
    axe = "mcl_core:axe_diamond"
    shovel = "mcl_core:shovel_diamond"
    fuse = "mesecons:redstone"
end

minetest.register_craft({
    output = "omni_tool:axe",
    recipe = {
        {pick, axe, shovel},
        {air, fuse, air},
        {air, fuse, air}
    }
})

minetest.register_craft({
    output = "omni_tool:axe",
    recipe = {
        {axe, pick, shovel},
        {air, fuse, air},
        {air, fuse, air}
    }
})

minetest.register_craft({
    output = "omni_tool:axe",
    recipe = {
        {axe, shovel, pick},
        {air, fuse, air},
        {air, fuse, air}
    }
})

minetest.register_craft({
    output = "omni_tool:axe",
    recipe = {
        {shovel, axe, pick},
        {air, fuse, air},
        {air, fuse, air}
    }
})

minetest.register_craft({
    output = "omni_tool:axe",
    recipe = {
        {shovel, pick, axe},
        {air, fuse, air},
        {air, fuse, air}
    }
})

minetest.register_craft({
    output = "omni_tool:axe",
    recipe = {
        {pick, shovel, axe},
        {air, fuse, air},
        {air, fuse, air}
    }
})
