# Omni Tool

3 tools in 1.

To switch using 'auto-switch', just right click on the designated node you want to break, the tool will switch to the correct tool that can mine it the fastest.

> You can manually switch via holding the SNEAK (default left shift) while right clicking (on either a node or air) to cycle thru (in the pattern of Axe -> Pickaxe -> Shovel -> Axe).

Please note however, the tool shares it's durability among all 3 states. (As such it comes with a hefty durability)

The tool tries not to interfere with nodes like doors, chests, and other nodes that might have a use on right click (like opening an inventory or formspec)

The Pickaxe can now mine invisible walls from mods like [invisible_wall by X17](https://content.minetest.net/packages/X17/invisible_wall/).

The tool can now be used with [toolranks by lisacvuk](https://content.minetest.net/packages/lisacvuk/toolranks/).

# Credits & Acknowledgements

> Because we use modified versions of [MineClone2](https://content.minetest.net/packages/Wuzzy/mineclone2/)'s textures the mod is under [GPL-3.0 Only](https://spdx.org/licenses/GPL-3.0-only.html)

- `textures/omni_tool_axe.png` is a modified version of `mcl_tool_steelaxe.png` which is from the [MineClone2](https://content.minetest.net/packages/Wuzzy/mineclone2/) game (Under a [GPL-3.0 Only](https://spdx.org/licenses/GPL-3.0-only.html))
- `textures/omni_tool_pickaxe.png` is a modified version of `mcl_tool_steelpick.png` which is from the [MineClone2](https://content.minetest.net/packages/Wuzzy/mineclone2/) game (Under a [GPL-3.0 Only](https://spdx.org/licenses/GPL-3.0-only.html))
- `textures/omni_tool_shovel.png` is a modified version of `mcl_tool_steelshovel.png` which is from the [MineClone2](https://content.minetest.net/packages/Wuzzy/mineclone2/) game (Under a [GPL-3.0 Only](https://spdx.org/licenses/GPL-3.0-only.html))
